<?php

	class Building {
		protected $name;
		protected $floors;
		protected $address;

		public function __construct($name, $floors, $tirahan) {
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $tirahan;
		}
	}

	class Condominium extends Building{
		//Encapsulation - getter
		public function getName() {
			return $this->name;
		}

		//Encapsulation - setter
		public function setName($name) {
			$this->name = $name;
		}
	}

	$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

	$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

