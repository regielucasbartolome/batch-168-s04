<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S04: Access Modifiers and Encapsulation</title>
</head>
<body>
	<h1>Access Modifiers</h1>

	<h2>Building Variables</h2>

	<p><?php //echo $building->name; ?></p>

	<h2>Condominium Variables</h2>

	<p><?php // echo $condominium->name; ?></p>

	<h1>Encapsulation</h1>

	<!-- use of getter -->
	<p>The name of the condominium is <?php echo $condominium->getname(); ?></p>

	<!-- use of setter -->
	<?php $condominium->setName('Enzo Tower'); ?>

	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?> </p>
</body>
</html>