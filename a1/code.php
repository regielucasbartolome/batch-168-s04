<?php

	class Building {
		protected $name;
		protected $floors;
		protected $address;

		public function __construct($name, $floors, $tirahan) {
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $tirahan;
		}

		public function getName() {
			return $this->name;
		}

		public function setName($name) {
			$this->name = $name;
		}

		public function getFloors() {
			return $this->floors;
		}

		public function getAddress() {
			return $this->address;
		}

	}

	class Condominium extends Building{
		//Encapsulation - getter
		public function getName() {
			return $this->name;
		}

		//Encapsulation - setter
		public function setName($name) {
			$this->name = $name;
		}

		//Encapsulation - getter
		public function getFloors() {
			return $this->floors;
		}

		//Encapsulation - getter
		public function getAddress() {
			return $this->address;
		}


	}

	$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

	$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

